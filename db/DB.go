package db

import (
	"gitlab.com/AgentNemo/signature-service/logging"
	"gorm.io/gorm"
)

type DB struct {
	*gorm.DB
}

func NewDB(logger logging.Logger) (*DB, error) {
	db, err := gorm.Open(NewPostgresByEnv(), &gorm.Config{
		Logger: logger,
	})
	if err != nil {
		return nil, err
	}
	return &DB{db}, nil
}

func NewDBWIthDialector(dialector gorm.Dialector, config gorm.Config) (*DB, error) {
	db, err := gorm.Open(dialector, &config)
	if err != nil {
		return nil, err
	}
	return &DB{db}, nil
}

func (d *DB) ApplySchemas(schemas ...interface{}) {
	d.DB.AutoMigrate(schemas...)
}
