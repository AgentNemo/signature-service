package types

import (
	"database/sql/driver"
	"encoding/json"
)

type JSON map[string]interface{}

func (j JSON) Value() (driver.Value, error) {
	valueString, err := json.Marshal(j)
	return string(valueString), err
}

func (j *JSON) Scan(value interface{}) error {
	if err := json.Unmarshal(value.([]byte), &j); err != nil {
		return err
	}
	return nil
}

func (j *JSON) ToTyped() map[string]string {
	ret := make(map[string]string, 0)
	for key, value := range *j {
		ret[key] = value.(string)
	}
	return ret
}

func (j *JSON) FromTyped(input map[string]string) JSON {
	for key, value := range input {
		(*j)[key] = value
	}
	return *j
}
