package models

import (
	"gitlab.com/AgentNemo/signature-service/db"
	"gitlab.com/AgentNemo/signature-service/db/types"
)

type Client struct {
	ID           uint `json:"-"`
	TSSID        uint
	UUID         string
	SerialNumber string
	Metadata     types.JSON
	DB           *db.DB `json:"-" gorm:"-" sql:"-"`
}

func NewClient(DB *db.DB) *Client {
	return &Client{DB: DB}
}

func (c *Client) SetTSSID(id uint) *Client {
	c.TSSID = id
	return c
}

func (c *Client) SetUUID(uuid string) *Client {
	c.UUID = uuid
	return c
}

func (c *Client) SetMetadata(metadata types.JSON) *Client {
	c.Metadata = metadata
	return c
}

func (c *Client) SetSerialNumber(serialNumber string) *Client {
	c.SerialNumber = serialNumber
	return c
}

func (c *Client) Create() error {
	return c.DB.Create(c).Error
}

func (c *Client) Read() error {
	return c.DB.Where(c).First(c).Error
}
