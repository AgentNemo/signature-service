package models

import (
	"github.com/1william1/ecc"
	"gitlab.com/AgentNemo/signature-service/db"
	"gitlab.com/AgentNemo/signature-service/db/types"
)

type Transaction struct {
	ID       uint `json:"-"`
	ClientID uint
	UUID     string
	Data     []byte
	Metadata types.JSON
	Keys     ecc.Private
	DB       *db.DB `json:"-" gorm:"-" sql:"-"`
}

func NewTransaction(DB *db.DB) *Transaction {
	return &Transaction{DB: DB}
}

func (t *Transaction) SetClientID(id uint) *Transaction {
	t.ClientID = id
	return t
}

func (t *Transaction) SetUUID(uuid string) *Transaction {
	t.UUID = uuid
	return t
}

func (t *Transaction) SetMetadata(metadata types.JSON) *Transaction {
	t.Metadata = metadata
	return t
}

func (t *Transaction) SetData(data []byte) *Transaction {
	t.Data = data
	return t
}

func (t *Transaction) SetKeys(keys ecc.Private) *Transaction {
	t.Keys = keys
	return t
}

func (t *Transaction) Create() error {
	return t.DB.Create(t).Error
}

func (t *Transaction) Read() error {
	return t.DB.Where(t).First(t).Error
}
