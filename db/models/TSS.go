package models

import (
	"gitlab.com/AgentNemo/signature-service/db"
	"gitlab.com/AgentNemo/signature-service/db/types"
)

type TSS struct {
	ID           uint `json:"-"`
	UUID         string
	SerialNumber string
	Metadata     types.JSON
	DB           *db.DB `json:"-" gorm:"-" sql:"-"`
}

func NewTSS(db *db.DB) *TSS {
	return &TSS{
		DB: db,
	}
}

func (d *TSS) SetUUID(uuid string) *TSS {
	d.UUID = uuid
	return d
}

func (d *TSS) SetMetadata(metadata types.JSON) *TSS {
	d.Metadata = metadata
	return d
}

func (d *TSS) Create() error {
	return d.DB.Create(d).Error
}

func (d *TSS) Read() error {
	return d.DB.Where(d).First(d).Error
}
