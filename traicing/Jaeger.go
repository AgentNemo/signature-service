package traicing

import (
	"github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-client-go"
	"github.com/uber/jaeger-client-go/config"
	jaegerlog "github.com/uber/jaeger-client-go/log"
	"github.com/uber/jaeger-lib/metrics/prometheus"
	"io"
	"log"
	"os"
)

type Jaeger struct {
	Closer io.Closer
}

func NewJaeger() *Jaeger {
	j := &Jaeger{}
	metricsFactory := prometheus.New()

	configT, err := (&config.Configuration{}).FromEnv()
	if err != nil {
		log.Fatal(err)
	}
	configT.Reporter = &config.ReporterConfig{
		LogSpans: true,
	}
	configT.ServiceName = os.Getenv("SERVICE_NAME")
	configT.Sampler = &config.SamplerConfig{
		Type:  jaeger.SamplerTypeConst,
		Param: 1,
	}
	tracer, closer, err := configT.NewTracer(
		config.Metrics(metricsFactory),
		config.Logger(jaegerlog.StdLogger),
	)
	if err != nil {
		log.Fatal(err)
	}
	j.Closer = closer
	opentracing.SetGlobalTracer(tracer)
	return j
}
func (j *Jaeger) Close() {
	if j.Closer == nil {
		return
	}
	j.Closer.Close()
}
