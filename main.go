package main

import (
	"gitlab.com/AgentNemo/signature-service/api"
	"gitlab.com/AgentNemo/signature-service/db"
	"gitlab.com/AgentNemo/signature-service/db/models"
	"gitlab.com/AgentNemo/signature-service/logging"
	"gitlab.com/AgentNemo/signature-service/traicing"
	"log"
)

func main() {
	logger := logging.NewZapByEnv()
	opentracer := traicing.NewJaeger()
	defer opentracer.Closer.Close()

	database, err := db.NewDB(logger)
	if err != nil {
		log.Fatal(err)
	}
	database.ApplySchemas(&models.TSS{}, &models.Client{}, &models.Transaction{})

	err = api.StartAPI(logger, database)
	log.Fatal(err)
}
