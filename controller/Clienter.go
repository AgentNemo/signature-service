package controller

import (
	"context"
	"gitlab.com/AgentNemo/signature-service/api/models"
	"gitlab.com/AgentNemo/signature-service/db"
	models2 "gitlab.com/AgentNemo/signature-service/db/models"
	"gitlab.com/AgentNemo/signature-service/db/types"
	"gitlab.com/AgentNemo/signature-service/logging"
	"time"
)

type Clienter struct {
	DB *db.DB
}

func NewClienter(DB *db.DB) *Clienter {
	return &Clienter{DB: DB}
}

func (c *Clienter) Register(logger logging.Logger, tssID string, clientID string, input models.PUTClient) (*Information, error) {
	logger = logger.StartContext(context.Background(), "client register")
	defer logger.FinishContext()
	tss := models2.NewTSS(c.DB).SetUUID(tssID)
	err := tss.Read()
	if err != nil {
		return nil, ErrorNotFound
	}
	client := models2.NewClient(c.DB).SetTSSID(tss.ID).
		SetMetadata((&types.JSON{}).FromTyped(models.MetadataToMap(input.Metadata))).SetUUID(clientID).
		SetSerialNumber(input.SerialNumber)
	err = client.Create()
	if err != nil {
		return nil, ErrorCreate
	}
	logger.InfoW("created new client", "client", client.UUID)
	return &Information{
		ID:        clientID,
		Type:      "Client",
		CreatedAt: time.Now().UnixNano(),
		Metadata:  client.Metadata.ToTyped(),
	}, nil
}

func (c *Clienter) Get(logger logging.Logger, tssID string, clientID string) (*Information, error) {
	logger = logger.StartContext(context.Background(), "client get")
	defer logger.FinishContext()
	tss := models2.NewTSS(c.DB).SetUUID(tssID)
	err := tss.Read()
	if err != nil {
		return nil, ErrorNotFound
	}
	client := models2.NewClient(c.DB).SetTSSID(tss.ID).SetUUID(clientID)
	err = client.Read()
	if err != nil {
		return nil, ErrorNotFound
	}
	return &Information{
		ID:        clientID,
		Type:      "Client",
		CreatedAt: time.Now().UnixNano(),
		Metadata:  client.Metadata.ToTyped(),
	}, nil
}
