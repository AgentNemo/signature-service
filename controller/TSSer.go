package controller

import (
	"context"
	"gitlab.com/AgentNemo/signature-service/api/models"
	"gitlab.com/AgentNemo/signature-service/db"
	models2 "gitlab.com/AgentNemo/signature-service/db/models"
	"gitlab.com/AgentNemo/signature-service/db/types"
	"gitlab.com/AgentNemo/signature-service/logging"
	"time"
)

type TSSer struct {
	DB *db.DB
}

func NewTSSer(db *db.DB) *TSSer {
	return &TSSer{DB: db}
}

func (s *TSSer) Register(logger logging.Logger, id string, input models.PUTTSSInput) (*Information, error) {
	logger = logger.StartContext(context.Background(), "tss register")
	defer logger.FinishContext()
	tss := models2.NewTSS(s.DB)
	err := tss.SetUUID(id).SetMetadata((&types.JSON{}).FromTyped(models.MetadataToMap(input.Metadata))).Create()
	if err != nil {
		logger.ErrW(err)
		return nil, ErrorCreate
	}
	logger.InfoW("created new tss", "tss", tss.UUID)
	return &Information{
		ID:        id,
		Type:      "TSS",
		CreatedAt: time.Now().UnixNano(),
		Metadata:  tss.Metadata.ToTyped(),
	}, nil
}

func (s *TSSer) GetTSS(logger logging.Logger, id string) (*Information, error) {
	logger = logger.StartContext(context.Background(), "tss get")
	defer logger.FinishContext()
	tss := models2.NewTSS(s.DB).SetUUID(id)
	err := tss.Read()
	if err != nil {
		return nil, ErrorNotFound
	}
	return &Information{
		ID:        id,
		Type:      "TSS",
		CreatedAt: time.Now().UnixNano(),
		Metadata:  tss.Metadata.ToTyped(),
	}, nil
}
