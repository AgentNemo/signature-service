package controller

type Information struct {
	ID        string            `json:"id"`
	Type      string            `json:"type"`
	CreatedAt int64             `json:"created_at"`
	Metadata  map[string]string `json:"metadata"`
}
