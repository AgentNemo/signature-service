package controller

import (
	"context"
	"crypto/elliptic"
	"fmt"
	"github.com/1william1/ecc"
	"gitlab.com/AgentNemo/signature-service/api/models"
	"gitlab.com/AgentNemo/signature-service/db"
	models2 "gitlab.com/AgentNemo/signature-service/db/models"
	"gitlab.com/AgentNemo/signature-service/db/types"
	"gitlab.com/AgentNemo/signature-service/logging"
)

type Transactioner struct {
	DB    *db.DB
	Curve elliptic.Curve
}

func NewTransactioner(DB *db.DB) *Transactioner {
	return &Transactioner{DB: DB, Curve: elliptic.P256()}
}

func (t *Transactioner) Start(logger logging.Logger, tssID string, txID string, input models.PUTTransaction) (*Information, error) {
	logger = logger.StartContext(context.Background(), "transaction start")
	logger.FinishContext()
	tss := models2.NewTSS(t.DB).SetUUID(tssID)
	err := tss.Read()
	if err != nil {
		return nil, ErrorNotFound
	}
	client := models2.NewClient(t.DB).SetTSSID(tss.ID).SetUUID(input.ClientID)
	err = client.Read()
	if err != nil {
		return nil, ErrorNotFound
	}

	transaction := models2.NewTransaction(t.DB).SetClientID(client.ID).
		SetMetadata((&types.JSON{}).FromTyped(models.MetadataToMap(input.Metadata))).SetUUID(txID)

	key, err := ecc.GenerateKey(t.Curve)
	if err != nil {
		return nil, ErrorCreate
	}

	dataEncrypted, err := key.Public.Encrypt([]byte(fmt.Sprintf("%s", input.Input)))
	if err != nil {
		return nil, ErrorCreate
	}

	transaction.SetData(dataEncrypted).SetKeys(*key)

	err = transaction.Create()
	if err != nil {
		return nil, ErrorCreate
	}

	return nil, nil
}

func (t *Transactioner) Update() {

}

func (t *Transactioner) End() {

}
