package controller

import "errors"

var (
	ErrorCreate         = errors.New("error during creation")
	ErrorNotFound       = errors.New("not found")
	ErrorDeviceNotFound = errors.New("device not found")
)
