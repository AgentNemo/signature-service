module gitlab.com/AgentNemo/signature-service

go 1.16

require (
	github.com/1william1/ecc v1.0.1
	github.com/HdrHistogram/hdrhistogram-go v1.1.2 // indirect
	github.com/cockroachdb/errors v1.8.6
	github.com/go-chi/chi/v5 v5.0.5
	github.com/go-chi/httptracer v0.3.0
	github.com/go-playground/validator/v10 v10.9.0
	github.com/opentracing/opentracing-go v1.2.0
	github.com/prometheus/client_golang v1.11.0 // indirect
	github.com/uber/jaeger-client-go v2.29.1+incompatible
	github.com/uber/jaeger-lib v2.4.1+incompatible
	go.uber.org/zap v1.13.0
	gorm.io/driver/postgres v1.2.1
	gorm.io/gorm v1.22.2
)
