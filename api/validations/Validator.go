package validations

import (
	"context"
	"encoding/json"
	"github.com/cockroachdb/errors"
	"github.com/go-playground/validator/v10"
	"gitlab.com/AgentNemo/signature-service/api/models"
	"gitlab.com/AgentNemo/signature-service/logging"
	"io"
	"net/http"
)

var (
	ErrorJson       = errors.New("json error")
	ErrorValidation = errors.New("validation error")
)

type Validator func(payload io.ReadCloser, validator *validator.Validate, logger logging.Logger) (interface{}, error)
type CustomHandlerFunc func(http.ResponseWriter, *http.Request, interface{})

func ValidatorPUTTSS() Validator {
	return func(payload io.ReadCloser, validator *validator.Validate, logger logging.Logger) (interface{}, error) {
		logger.StartContext(context.Background(), "validation tss")
		defer logger.FinishContext()
		data := models.PUTTSSInput{}
		err := json.NewDecoder(payload).Decode(&data)
		if err != nil {
			logger.ErrW(err)
			return nil, ErrorJson
		}
		err = validator.Struct(data)
		if err != nil {
			logger.ErrW(err)
			return nil, ErrorValidation
		}
		return data, nil
	}
}

func ValidatorPUTTransaction() Validator {
	return func(payload io.ReadCloser, validator *validator.Validate, logger logging.Logger) (interface{}, error) {
		logger.StartContext(context.Background(), "validation transaction")
		defer logger.FinishContext()
		data := models.PUTTransaction{}
		err := json.NewDecoder(payload).Decode(&data)
		if err != nil {
			logger.ErrW(err)
			return nil, ErrorJson
		}
		err = validator.Struct(data)
		if err != nil {
			logger.ErrW(err)
			return nil, ErrorValidation
		}
		return data, nil
	}
}

func ValidatorPUTClient() Validator {
	return func(payload io.ReadCloser, validator *validator.Validate, logger logging.Logger) (interface{}, error) {
		logger.StartContext(context.Background(), "validation client")
		defer logger.FinishContext()
		data := models.PUTClient{}
		err := json.NewDecoder(payload).Decode(&data)
		if err != nil {
			logger.ErrW(err)
			return nil, ErrorJson
		}
		err = validator.Struct(data)
		if err != nil {
			logger.ErrW(err)
			return nil, ErrorValidation
		}
		return data, nil
	}
}
