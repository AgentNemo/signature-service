package utils

import (
	"gitlab.com/AgentNemo/signature-service/controller"
	"net/http"
)

func HandleError(err error, writer http.ResponseWriter) {
	switch err {
	case controller.ErrorCreate:
		writer.WriteHeader(http.StatusConflict)
	case controller.ErrorDeviceNotFound:
		writer.WriteHeader(http.StatusNotFound)
	case controller.ErrorNotFound:
		writer.WriteHeader(http.StatusNoContent)
	default:
		writer.WriteHeader(http.StatusInternalServerError)
	}
}
