package router

import (
	"errors"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/httptracer"
	"github.com/go-playground/validator/v10"
	"github.com/opentracing/opentracing-go"
	"gitlab.com/AgentNemo/signature-service/api/middlewares"
	"gitlab.com/AgentNemo/signature-service/api/validations"
	"gitlab.com/AgentNemo/signature-service/logging"
	"net/http"
	"os"
)

type Router struct {
	*chi.Mux
	Logger    logging.Logger
	Config    Config
	Validator *validator.Validate
}

func NewRouter(logger logging.Logger, config Config) *Router {
	return &Router{chi.NewRouter(), logger, config, validator.New()}
}

func NewRouterDefault(logger logging.Logger) *Router {
	router := NewRouter(logger, getConfigByEnv())
	router.setMiddlewareDefault()
	return router
}

func getConfigByEnv() Config {
	return Config{Port: os.Getenv("ROUTER_PORT")}
}

func (r *Router) setMiddlewareDefault() {
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middlewares.Logger(r.Logger))
	r.Use(middleware.Recoverer)
	r.Use(httptracer.Tracer(opentracing.GlobalTracer(), httptracer.Config{
		ServiceName:    os.Getenv("SERVICE_NAME"),
		ServiceVersion: os.Getenv("SERVICE_VERSION"),
		OperationName:  os.Getenv("SERVICE_NAME"),
		SkipFunc: func(r *http.Request) bool {
			return r.URL.Path == "/health"
		},
		Tags: nil,
	}))
}

func (r *Router) Start() error {
	return http.ListenAndServe(":10005", r)
}

func (r Router) PutWithValidation(pattern string, handlerFn validations.CustomHandlerFunc, validator validations.Validator) {
	r.Put(pattern, r.wrapValidate(validator, handlerFn))
}

func (r Router) wrapValidate(validator validations.Validator, handlerFn validations.CustomHandlerFunc) func(http.ResponseWriter, *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		data, err := validator(request.Body, r.Validator, r.Logger)
		if err != nil {
			if errors.Is(err, validations.ErrorJson) {
				writer.WriteHeader(http.StatusUnsupportedMediaType)
				return
			}
			if errors.Is(err, validations.ErrorValidation) {
				writer.WriteHeader(http.StatusUnprocessableEntity)
				return
			}
			writer.WriteHeader(http.StatusBadRequest)
		}
		handlerFn(writer, request, data)
	}
}
