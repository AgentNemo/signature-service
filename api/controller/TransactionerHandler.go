package controller

import (
	"gitlab.com/AgentNemo/signature-service/controller"
	"gitlab.com/AgentNemo/signature-service/logging"
	"net/http"
)

type TransactionerHandler struct {
	Logger        logging.Logger
	Transactioner *controller.Transactioner
}

func NewTransactionerHandler(logger logging.Logger, transactioner *controller.Transactioner) *TransactionerHandler {
	return &TransactionerHandler{Logger: logger, Transactioner: transactioner}
}

func (h TransactionerHandler) HandleTransaction(writer http.ResponseWriter, request *http.Request, input interface{}) {

}
