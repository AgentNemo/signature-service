package controller

import (
	"encoding/json"
	"github.com/go-chi/chi/v5"
	"gitlab.com/AgentNemo/signature-service/api/models"
	"gitlab.com/AgentNemo/signature-service/api/utils"
	"gitlab.com/AgentNemo/signature-service/controller"
	"gitlab.com/AgentNemo/signature-service/logging"
	"net/http"
	"os"
)

type ClienterHandler struct {
	Logger   logging.Logger
	Clienter *controller.Clienter
}

func NewClienterHandler(logger logging.Logger, clienter *controller.Clienter) *ClienterHandler {
	return &ClienterHandler{Logger: logger, Clienter: clienter}
}

func (h *ClienterHandler) RegisterNew(writer http.ResponseWriter, request *http.Request, payload interface{}) {
	logger := h.Logger.StartContext(request.Context(), "API Client Register")
	defer logger.FinishContext()
	tssID := chi.URLParam(request, "tss_id")
	clientID := chi.URLParam(request, "client_id")
	input := payload.(models.PUTClient)
	newCLient, err := h.Clienter.Register(h.Logger, tssID, clientID, input)
	if err != nil {
		utils.HandleError(err, writer)
		return
	}
	response := (&models.Information{
		ID:        newCLient.ID,
		Version:   os.Getenv("SERVICE_VERSION"),
		Type:      newCLient.Type,
		Stage:     os.Getenv("SERVICE_STAGE"),
		CreatedAt: newCLient.CreatedAt,
		Metadata:  newCLient.Metadata,
	}).FillServiceInfo()
	err = json.NewEncoder(writer).Encode(response)
	if err != nil {
		logger.ErrW(err, "tss_id", tssID)
	}
}

func (h *ClienterHandler) Get(writer http.ResponseWriter, request *http.Request) {
	logger := h.Logger.StartContext(request.Context(), "API Client Get")
	defer logger.FinishContext()
	tssID := chi.URLParam(request, "tss_id")
	clientID := chi.URLParam(request, "client_id")
	client, err := h.Clienter.Get(h.Logger, tssID, clientID)
	if err != nil {
		utils.HandleError(err, writer)
		return
	}
	response := (&models.Information{
		ID:        client.ID,
		Version:   os.Getenv("SERVICE_VERSION"),
		Type:      client.Type,
		Stage:     os.Getenv("SERVICE_STAGE"),
		CreatedAt: client.CreatedAt,
		Metadata:  client.Metadata,
	}).FillServiceInfo()
	err = json.NewEncoder(writer).Encode(response)
	if err != nil {
		logger.ErrW(err, "tss_id", tssID)
	}
}
