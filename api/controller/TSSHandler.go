package controller

import (
	"encoding/json"
	"github.com/go-chi/chi/v5"
	"gitlab.com/AgentNemo/signature-service/api/models"
	"gitlab.com/AgentNemo/signature-service/api/utils"
	"gitlab.com/AgentNemo/signature-service/controller"
	"gitlab.com/AgentNemo/signature-service/logging"
	"net/http"
)

type TSSHandler struct {
	Logger  logging.Logger
	Devicer *controller.TSSer
}

func NewTSSHandler(logger logging.Logger, Devicer *controller.TSSer) *TSSHandler {
	return &TSSHandler{Logger: logger, Devicer: Devicer}
}

func (h *TSSHandler) RegisterNew(writer http.ResponseWriter, request *http.Request, payload interface{}) {
	logger := h.Logger.StartContext(request.Context(), "API TSS Register")
	defer logger.FinishContext()
	tssID := chi.URLParam(request, "tss_id")
	input := payload.(models.PUTTSSInput)
	newDevice, err := h.Devicer.Register(logger, tssID, input)
	if err != nil {
		logger.ErrW(err, "tss_id", tssID)
		utils.HandleError(err, writer)
		return
	}
	response := (&models.Information{
		ID:        newDevice.ID,
		Type:      newDevice.Type,
		CreatedAt: newDevice.CreatedAt,
		Metadata:  newDevice.Metadata,
	}).FillServiceInfo()
	err = json.NewEncoder(writer).Encode(response)
	if err != nil {
		logger.ErrW(err, "tss_id", tssID)
	}
}

func (h *TSSHandler) GetInformation(writer http.ResponseWriter, request *http.Request) {
	logger := h.Logger.StartContext(request.Context(), "API TSS Get")
	defer logger.FinishContext()
	tssID := chi.URLParam(request, "tss_id")
	device, err := h.Devicer.GetTSS(logger, tssID)
	if err != nil {
		logger.ErrW(err, "tss_id", tssID)
		utils.HandleError(err, writer)
		return
	}
	response := (&models.Information{
		ID:        device.ID,
		Type:      device.Type,
		CreatedAt: device.CreatedAt,
		Metadata:  device.Metadata,
	}).FillServiceInfo()
	err = json.NewEncoder(writer).Encode(response)
	if err != nil {
		logger.ErrW(err, "tss_id", tssID)
	}
}
