package api

import (
	apiController "gitlab.com/AgentNemo/signature-service/api/controller"
	"gitlab.com/AgentNemo/signature-service/api/router"
	"gitlab.com/AgentNemo/signature-service/api/validations"
	"gitlab.com/AgentNemo/signature-service/controller"
	"gitlab.com/AgentNemo/signature-service/db"
	"gitlab.com/AgentNemo/signature-service/logging"
)

func StartAPI(logger logging.Logger, database *db.DB) error {
	signaturer := controller.NewTSSer(database)
	handlerDevice := apiController.NewTSSHandler(logger, signaturer)

	transactioner := controller.NewTransactioner(database)
	handlerTransactioner := apiController.NewTransactionerHandler(logger, transactioner)

	clienter := controller.NewClienter(database)
	handlerClienter := apiController.NewClienterHandler(logger, clienter)

	r := router.NewRouterDefault(logger)

	r.PutWithValidation("/{tss_id}", handlerDevice.RegisterNew, validations.ValidatorPUTTSS())

	r.PutWithValidation("/tss/{tss_id}/tx/{tx_id_or_number}", handlerTransactioner.HandleTransaction,
		validations.ValidatorPUTTransaction())

	r.PutWithValidation("/tss/{tss_id}/client/{client_id}", handlerClienter.RegisterNew,
		validations.ValidatorPUTClient())

	return r.Start()
}
