package middlewares

import (
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/AgentNemo/signature-service/logging"
	"net/http"
)

func Logger(logger logging.Logger) func(next http.Handler) http.Handler {
	return middleware.RequestLogger(&middleware.DefaultLogFormatter{Logger: logger, NoColor: true})
}
