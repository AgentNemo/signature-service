package models

type PUTTransaction struct {
	State    string     `validator:"required,oneof=ACTIVE CANCELLED FINISHED"`
	ClientID string     `validator:"required"`
	Metadata []Metadata `json:"metadata" validate:"dive"`
	Input    interface{}
}
