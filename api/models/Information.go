package models

import "os"

type Information struct {
	ID        string            `json:"id"`
	Version   string            `json:"version"`
	Type      string            `json:"type"`
	Stage     string            `json:"stage"`
	CreatedAt int64             `json:"created_at"`
	Metadata  map[string]string `json:"metadata"`
}

func (i *Information) FillServiceInfo() *Information {
	i.Version = os.Getenv("SERVICE_VERSION")
	i.Stage = os.Getenv("SERVICE_STAGE")
	return i
}
