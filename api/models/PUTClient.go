package models

type PUTClient struct {
	SerialNumber string     `json:"serial_number" validator:"alphanum,min=1,max=100"`
	Metadata     []Metadata `json:"metadata" validate:"dive"`
}
