package models

type Metadata struct {
	Name  string `json:"name" validate:"required,min=1,max=40"`
	Value string `json:"value" validate:"required,min=1,max=500"`
}

func MetadataToMap(metadatas []Metadata) map[string]string {
	ret := make(map[string]string, 0)
	for _, data := range metadatas {
		ret[data.Name] = data.Value
	}
	return ret
}
